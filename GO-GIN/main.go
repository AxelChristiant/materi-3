package main

import (
	"log"
	"pustaka-api/book"
	"pustaka-api/handler"

	"github.com/gin-gonic/gin"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	dsn := "root:@tcp(127.0.0.1:3306)/pustaka-api?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	router := gin.Default()

	if err != nil {
		log.Fatal("Db connection error")
	}
	db.AutoMigrate(&book.Book{})
	bookRepository := book.NewRepository(db)
	// bookFileRepository := book.NewFileRepository()
	bookService := book.NewService(bookRepository)
	bookHandler := handler.NewBookHandler(bookService)

	//membuat versioning
	v1 := router.Group("/v1")

	v1.GET("/", bookHandler.RootHandler)

	v1.GET("/books", bookHandler.GetBooks)

	v1.GET("books/:id", bookHandler.GetBook)

	v1.GET("/books/:id/:title", bookHandler.BooksHandler)

	v1.GET("/query", bookHandler.QueryHandler)

	v1.POST("/books", bookHandler.CreateBookHandler)

	v1.PUT("/books/:id", bookHandler.UpdateBookHandler)
	v1.DELETE("/books/:id", bookHandler.DeleteBookHandler)

	router.Run(":8080")

	//main
	//handler
	//service
	//repository
	//db
	//mysql

}
