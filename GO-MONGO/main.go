package main

import (
	"fmt"
	"net/http"
	"time"

	"GO-MONGO/controllers"

	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2"
)

func main() {
	r := httprouter.New()
	uc := controllers.NewUserController(getSession())
	r.GET("/user/:id", uc.GetUser)
	r.POST("/user", uc.CreateUser)
	r.DELETE("/user/:id", uc.DeleteUser)
	http.ListenAndServe("localhost:9000", r)
}

func getSession() *mgo.Session {
	maxWait := time.Duration(5 * time.Second)
	s, err := mgo.DialWithTimeout("mongodb://localhost:27017", maxWait)
	if err != nil {
		fmt.Println(err)
		panic(err)

	}
	fmt.Println("Database Connected")
	return s
}
